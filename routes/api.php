<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\BoardMemberController;
use App\Http\Controllers\Api\HomeBannerController;
use App\Http\Controllers\Api\AboutSectionController;
use App\Http\Controllers\Api\AboutPanelController;
use App\Http\Controllers\Api\ImageController;
use App\Http\Controllers\Api\EducationController;
use App\Http\Controllers\Api\ActivityController;
use App\Http\Controllers\Api\ActivityImageController;
use App\Http\Controllers\Api\GalleryAlbumController;
use App\Http\Controllers\Api\GalleryImageController;
use App\Http\Controllers\Api\SettingsController;
use App\Http\Controllers\Api\SocialMediaController;
use App\Http\Controllers\Api\PartnerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Public Routes

// Users
Route::post('/login', [AuthController::class, 'login']);
// Home Page
Route::get('/cat_banners', [HomeBannerController::class, 'getHomeBanners']);
Route::get('/f_activities', [ActivityController::class, 'getFeaturedActivities']);
// Settings & Contact Page
Route::get('/cat_setting', [SettingsController::class, 'getSettings']);
// About Page
Route::prefix('/about')->group( function() {
    Route::get('/cat_sections', [AboutSectionController::class, 'getAllSections']);
    Route::get('/cat_panels', [AboutPanelController::class, 'getAllPanels']);
});
// Board Member Page
Route::get('/cat_bmembers', [BoardMemberController::class, 'getAllMembers']);
// Education Page
Route::get('/cat_lessons', [EducationController::class, 'getAllLessons']);
Route::get('/cat_lesson/{id}', [EducationController::class, 'getLesson']);
// Activity Page
Route::get('/cat_activities', [ActivityController::class, 'getAllActivities']);
Route::get('/cat_activity/{id}', [ActivityController::class, 'getActivity']);
// Gallery
Route::prefix('/gallery')->group( function() {
    Route::get('/cat_albums', [GalleryAlbumController::class, 'getAlbums']);
    Route::get('/cat_album/{id}', [GalleryAlbumController::class, 'getAlbum']);
});


// Protected Routes
Route::middleware('auth:sanctum')->group( function () {
    Route::post('/upload', [ImageController::class, 'uploadImage']);
    Route::post('/upload_file', [ImageController::class, 'uploadFile']);
    Route::post('/upload_many', [ImageController::class, 'uploadMultipleImage']);
    Route::post('/remove_upload', [ImageController::class, 'deleteFile']);
    
    // Users
    Route::post('/register', [AuthController::class, 'register']);
    Route::get('/current_user', [UserController::class, 'currentUser']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/users', [UserController::class, 'getAllUsers']);
    Route::prefix('/user')->group( function() {
        Route::patch('/{id}', [UserController::class, 'updateUser']);
        Route::delete('/{id}', [UserController::class, 'deleteUser']);
    });

    // Profile
    Route::patch('/profile/{id}', [UserController::class, 'updateProfile']);
    Route::patch('/profile/reset_password/{id}', [UserController::class, 'updateProfilePassword']);
    
    // Home Banners
    Route::get('/banners', [HomeBannerController::class, 'getAllBanners']);
    Route::prefix('/banner')->group( function() {
        Route::post('/create', [HomeBannerController::class, 'createBanner']);
        Route::patch('/{id}', [HomeBannerController::class, 'updateBanner']);
        Route::delete('/{id}', [HomeBannerController::class, 'deleteBanner']);
    });

    // About Content
    Route::prefix('/about')->group( function() {
        // Sections
        Route::get('/sections', [AboutSectionController::class, 'getAllSections']);
        Route::post('/section', [AboutSectionController::class, 'createSection']);
        Route::patch('/section/{id}', [AboutSectionController::class, 'updateSection']);
        Route::delete('/section/{id}', [AboutSectionController::class, 'deleteSection']);
        //Panels
        Route::get('/panels', [AboutPanelController::class, 'getAllPanels']);
        Route::post('/panel', [AboutPanelController::class, 'createPanel']);
        Route::patch('/panel/{id}', [AboutPanelController::class, 'updatePanel']);
        Route::delete('/panel/{id}', [AboutPanelController::class, 'deletePanel']);
        // Panel Section
        Route::post('/panel/section/{id}', [AboutPanelController::class, 'createPanelSection']);
        Route::patch('/panel/section/{id}', [AboutPanelController::class, 'updatePanelSection']);
        Route::delete('/panel/section/{id}', [AboutPanelController::class, 'deletePanelSection']);

    });

    // Board Members
    Route::get('/board_members', [BoardMemberController::class, 'getAllMembers']);
    Route::prefix('/board_member')->group( function() {
        Route::post('/create', [BoardMemberController::class, 'createMember']);
        Route::patch('/{id}', [BoardMemberController::class, 'updateMember']);
        Route::delete('/{id}', [BoardMemberController::class, 'deleteMember']);
    });

    // Educations
    Route::get('/lessons', [EducationController::class, 'getAllLessons']);
    Route::prefix('/lesson')->group( function() {
        Route::post('/create', [EducationController::class, 'createLesson']);
        Route::patch('/{id}', [EducationController::class, 'updateLesson']);
        Route::delete('/{id}', [EducationController::class, 'deleteLesson']);
    });
    
    // Activities
    Route::get('/activities', [ActivityController::class, 'getAllActivities']);
    Route::prefix('/activity')->group( function() {
        Route::post('/create', [ActivityController::class, 'createActivity']);
        Route::patch('/{id}', [ActivityController::class, 'updateActivity']);
        Route::delete('/{id}', [ActivityController::class, 'deleteActivity']);

        // Images
        Route::post('/image', [ActivityImageController::class, 'createImage']);
        Route::delete('/image/{id}', [ActivityImageController::class, 'deleteImage']);
    });


    // Gallery
    Route::prefix('/gallery')->group( function() {
        // Albums
        Route::get('/albums', [GalleryAlbumController::class, 'getAllAlbums']);
        Route::get('/album/{id}', [GalleryAlbumController::class, 'getAlbum']);
        Route::post('/album', [GalleryAlbumController::class, 'createAlbum']);
        Route::patch('/album/{id}', [GalleryAlbumController::class, 'updateAlbum']);
        Route::delete('/album/{id}', [GalleryAlbumController::class, 'deleteAlbum']);

        // Images
        Route::post('/image', [GalleryImageController::class, 'createImage']);
        Route::delete('/image/{id}', [GalleryImageController::class, 'deleteImage']);
    });

    // Settings (General and Social Media)
    Route::get('/general_settings', [SettingsController::class, 'getSettings']);
    Route::get('/social_medias', [SocialMediaController::class, 'showAllSocials']);
    Route::get('/partners', [PartnerController::class, 'showAllPartners']);
    Route::prefix('/setting')->group( function() {
        Route::patch('general/{id}', [SettingsController::class, 'saveSettings']);
        Route::post('social_media/create', [SocialMediaController::class, 'addSocial']);
        Route::patch('social_media/{id}', [SocialMediaController::class, 'updateSocial']);
        Route::delete('social_media/{id}', [SocialMediaController::class, 'deleteSocial']);
        Route::post('partner/create', [PartnerController::class, 'addPartner']);
        Route::patch('partner/{id}', [PartnerController::class, 'updatePartner']);
        Route::delete('partner/{id}', [PartnerController::class, 'deletePartner']);
    });

});
