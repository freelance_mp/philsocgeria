<template>
     <div class="setting-general-container component-container p-5">
          <div class="page-header mb-5">
               <h2 class="text-h5 font-weight-bold">Settings - General</h2>
               <v-btn outlined color="#6a994e" :loading="isSaving" @click="saveChanges" ><v-icon class="mr-2">mdi-content-save</v-icon>{{ isSaving ? "Saving..." : "Save changes" }}</v-btn>
          </div>
          <v-form>
               <v-row>
                    <!-- Logo -->
                    <v-col cols="2" sm="1" class="mb-5 pt-0" v-if="edit && imageHolder">
                         <v-img max-height="50" max-width="50" :src="imageHolder" :alt="data.name"></v-img>
                         <!-- <v-img max-height="50" max-width="50" src="/uploads/no_image.jpg" alt="no image" v-else></v-img> -->
                    </v-col>
                    <v-col cols="4" sm="5" class="mb-5 pt-0 d-flex align-items-center">
                         <v-btn @click="showUpload = true" v-if="imageHolder && !showUpload">
                              Change
                         </v-btn>
                         <div v-else>
                              <v-file-input
                                   ref="upload"
                                   :headers="{ 'x-csrf-token': token, 'X-Requested-With' : 'XMLHttpRequest'}"
                                   :rules="imageRules"
                                   accept="image/png, image/jpeg, image/jpg"
                                   label="Upload Logo"
                                   prepend-icon="mdi-camera"
                                   small-chips
                                   clear-icon="mdi-delete"
                                   @change="selectFile">
                              </v-file-input>
                         </div>
                    </v-col>
                    <!-- Name -->
                    <v-col cols="12" class="py-0">
                         <v-text-field v-model="data.name" label="Name" outlined :rules="nameRules" required></v-text-field>
                    </v-col>
                    <!-- Email -->
                    <v-col cols="12" class="py-0">
                         <v-text-field v-model="data.email" label="Email" outlined :rules="emailRules" required></v-text-field>
                    </v-col>
                    <!-- Contact -->
                    <v-col cols="12" class="py-0">
                         <v-textarea v-model="data.contact_number" auto-grow label="Contact" outlined rows="3"></v-textarea>
                    </v-col>
                    <!-- Address -->
                    <v-col cols="12" class="py-0">
                         <v-textarea v-model="data.address" auto-grow label="Address" outlined rows="3"></v-textarea>
                    </v-col>
                    <!-- Google Map -->
                    <v-col cols="12" class="py-0">
                         <v-textarea v-model="data.google_map" auto-grow label="Google Map" outlined rows="3"></v-textarea>
                    </v-col>
                    
               </v-row>
          </v-form>
          <!-- Snackbar -->
          <v-snackbar v-model="snackbar.display" :multi-line="snackbar.multiLine" :timeout="2000" top :color="snackbar.color">
               {{ snackbar.text }}
               <template v-slot:action="{ attrs }">
                    <v-btn text color="white" v-bind="attrs" @click="snackbar.display = false">Close</v-btn>
               </template>
          </v-snackbar>
     </div>
</template>
<script>
export default {
     data() {
          return {
               data: {
                    logo: null,
                    name: '',
                    email: '',
                    contact_number: '',
                    address: '',
                    google_map: '',
               },
               re: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
               nameRules: [
                    value => !!value || 'Name is required.',
                    value => (value && value.length > 3 ) || 'Name must be greater than 3 characters.',
               ],
               emailRules: [
                    value => !!value || 'Email is required.',
                    value => this.re.test(value) || 'Email must be valid',
               ],
               imageRules: [
                    value => !value || value.size < 2000000 || 'Avatar size should be less than 2 MB.',
                    value => (!value || (value.type == 'image/jpg' || value.type == 'image/png' || value.type == 'image/jpeg')) || 'The image format is invalid'
               ],
               isSaving: false,
               snackbar: {
                    display: false,
                    color: '',
                    text: '',
                    multiLine: true,
               },
               edit: true,
               imageHolder: null,
               showUpload: false,
          }
     },
     created() {
          this.token = window.Laravel.csrfToken;
          this.getSettings();
     },
     methods: {
          async getSettings() {
               const res = await this.callApi("get", "api/general_settings");
               if (res.status === 200) {
                    this.data = res.data;
                    this.imageHolder = res.data.logo ? res.data.logo : "/uploads/no_image.jpg";
               } else {
                    this.snackbar.text = "Something Went Wrong";
                    this.snackbar.color = "error";
                    this.snackbar.display = true;
               }
          },
          async saveChanges(){
               this.isSaving = true
               const res = await this.callApi("patch",`api/setting/general/${this.data.id}`,this.data);
               if (res.status === 200) {
                    this.snackbar.text = "Changes has been saved.";
                    this.snackbar.color = 'success';
                    this.snackbar.display = true;
                    this.showUpload = false;
                    this.getSettings();
               } else {
                    this.snackbar.color = "error";
                    if (res.status == 422) {
                         for (let i in res.data.errors) {
                              this.snackbar.text = res.data.errors[i][0];
                              this.snackbar.display = true;
                         }
                    } else {
                         this.snackbar.text = "Something Went Wrong";
                         this.snackbar.display = true;
                    }
               }
               this.isSaving = false
          },
          async selectFile(e){
               if(this.$refs.upload.validate() && e){
                    const data = new FormData();
                    data.append('file', e);
                    const res = await this.callApi("post", `api/upload`, data);
                    if( res.status == 200 ) {
                         this.data.logo = res.data;
                    }
               } else {
                    if( this.edit ){
                         this.data.logo = this.imageHolder;
                    } else {
                         this.data.logo = null;
                    }
               }
          },
     }

}
</script>