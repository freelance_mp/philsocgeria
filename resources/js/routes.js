import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

// Catalog
import CatalogHomePage from './views/catalog/HomePage.vue'
import CatalogAboutPage from './views/catalog/AboutPage.vue'
import CatalogBoardMemberPage from './views/catalog/BoardMemberPage.vue'
import CatalogEducationPage from './views/catalog/EducationPage.vue'
import CatalogEducationInnerPage from './views/catalog/EducationInnerPage.vue'
import CatalogActivityPage from './views/catalog/ActivityPage.vue'
import CatalogActivityInnerPage from './views/catalog/ActivityInnerPage.vue'
import CatalogGalleryPage from './views/catalog/GalleryPage.vue'
import CatalogGalleryInnerPage from './views/catalog/GalleryInnerPage.vue'
import CatalogContactPage from './views/catalog/ContactPage.vue'
import LoginPage from './views/catalog/LoginForm.vue'

// Admin
import DashboardPage from './views/admin/DashboardComponent.vue'
import BannerPage from './views/admin/BannerComponent.vue'
import AboutPanelPage from './views/admin/AboutPanelComponent.vue'
import AboutSectionPage from './views/admin/AboutSectionComponent.vue'
import BoardMembersPage from './views/admin/BoardMemberComponent.vue'
import EducationPage from './views/admin/EducationComponent.vue'
import ActivityPage from './views/admin/ActivityComponent.vue'
import GalleryAlbumPage from './views/admin/GalleryAlbumComponent.vue'
import GalleryImagePage from './views/admin/GalleryImageComponent.vue'
import UserPage from './views/admin/UserComponent.vue'
import GeneralSettingPage from './views/admin/GeneralSettingComponent.vue'
import SocialMediaPage from './views/admin/SocialMediaComponent.vue'
import PartnerPage from './views/admin/PartnerComponent.vue'
import ProfilePage from './views/admin/ProfileComponent.vue'

import DeleteModalPage from './views/admin/DeleteModalComponent.vue'

const routes = [
     // Catalog
     {
     path: '/login',
     component: LoginPage,
     name: 'user.login'
     },
     {
         path: '/home',
         component: CatalogHomePage,
         name: 'home'
     },
     {
          path: '/about',
          component: CatalogAboutPage,
          name: 'about'
     },
     {
          path: '/board-member',
          component: CatalogBoardMemberPage,
          name: 'bmember'
     },
     {
          path: '/education',
          component: CatalogEducationPage,
          name: 'education'
     },
     {
          path: '/education',
          component: CatalogEducationInnerPage,
          name: 'education.inner'
     },
     {
          path: '/activity',
          component: CatalogActivityPage,
          name: 'activity'
     },
     {
          path: '/activity',
          component: CatalogActivityInnerPage,
          name: 'activity.inner'
     },
     {
          path: '/gallery',
          component: CatalogGalleryPage,
          name: 'gallery'
     },
     {
          path: '/gallery',
          component: CatalogGalleryInnerPage,
          name: 'gallery.inner'
     },
     {
          path: '/contact-us',
          component: CatalogContactPage,
          name: 'contact'
     },
     // Admin
     {
          path: '/dashboard',
          component: DashboardPage,
          name: 'admin.dashboard'
     },
     {
          path: '/home-banners',
          component: BannerPage,
          name: 'admin.banner'
     },
     {
          path: '/about-sections',
          component: AboutSectionPage,
          name: 'admin.section'
     },
     {
          path: '/about-panels',
          component: AboutPanelPage,
          name: 'admin.panel'
     },
     {
          path: '/board-members',
          component: BoardMembersPage,
          name: 'admin.bmember'
     },
     {
          path: '/educations',
          component: EducationPage,
          name: 'admin.education'
     },
     {
          path: '/activities',
          component: ActivityPage,
          name: 'admin.activity'
     },
     {
          path: '/gallery-albums',
          component: GalleryAlbumPage,
          name: 'admin.album'
     },
     {
          path: '/gallery-images',
          component: GalleryImagePage,
          name: 'admin.gallery'
     },
     {
          path: '/user-accounts',
          component: UserPage,
          name: 'admin.user'
     },
     {
          path: '/settings-general',
          component: GeneralSettingPage,
          name: 'admin.general'
     },
     {
          path: '/settings-social-media',
          component: SocialMediaPage,
          name: 'admin.social'
     },
     {
          path: '/settings-partners',
          component: PartnerPage,
          name: 'admin.partner'
     },
     {
          path: '/profile',
          component: ProfilePage,
          name: 'admin.profile'
     },

     {
          path: '/delete-modal',
          components: DeleteModalPage,
     },

];

export default new VueRouter({
     mode: 'history',
     routes,
})