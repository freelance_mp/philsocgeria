require('./bootstrap');

window.Vue = require('vue').default;

import Vuetify from "../plugins/vuetify";
import routes from "./routes";
import store from './store';
import Vue2Editor from "vue2-editor";
import videojs from "video.js";
import common from './common';
import Vue from "vue";

Vue.mixin(common); // can be use in any component globally
Vue.use(Vue2Editor);
Vue.use(videojs);

Vue.component('main-app', require('./views/MainApp.vue').default);
Vue.component('catalog-app', require('./views/CatalogApp.vue').default);
Vue.component('login-form', require('./views/catalog/LoginForm.vue').default);



const app = new Vue({
    el: '#app',
    vuetify: Vuetify,
    router: routes,
    store
});
