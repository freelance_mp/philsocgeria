import Vue from "vue"
import Vuex from "vuex"

import currentUser from "./modules/currentUser"
import user from "./modules/user"
import lesson from "./modules/lesson"
import activity from "./modules/activity"
import banner from "./modules/banner"
import gallery from "./modules/gallery"
import deleteModal from "./modules/deleteModal"

Vue.use(Vuex);

export default new Vuex.Store({
     modules: {
          currentUser,
          user,
          lesson,
          activity,
          banner,
          gallery,
          deleteModal,
     }
});
 