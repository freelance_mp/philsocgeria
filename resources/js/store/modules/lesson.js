import axios from "axios";

const state = {
     lessons: {},
     lesson: {}
};

const getters = {};

const actions = {
     getList( {commit}, pageNumber) {
          axios.get("api/lessons?page=" + pageNumber )
          .then( response => {
               commit('setLessons', response.data );
          });
     },
     catalogGetList( {commit}, pageNumber) {
          axios.get("api/cat_lessons?page=" + pageNumber )
          .then( response => {
               commit('setLessons', response.data );
          });
     },
     getLesson( {commit}, id ) {
          axios.get("api/cat_lesson/" + id)
          .then( response => {
               commit('setLesson', response.data );
          });
     }
};

const mutations = {
     setLessons( state, data ) {
          state.lessons = data;
     },
     setCurrentPage( state, data ) {
          state.lessons.current_page = data;
     },
     setLesson( state, data ) {
          state.lesson = data;
     }
};

export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
}