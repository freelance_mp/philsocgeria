import axios from "axios";

const state = {
     activities: {},
     activity: {}
};

const getters = {};

const actions = {
     getList( {commit}, pageNumber) {
          axios.get("api/activities?page=" + pageNumber )
          .then( response => {
               commit('setActivities', response.data );
          });
     },
     catalogGetList( {commit}, pageNumber) {
          axios.get("api/cat_activities?page=" + pageNumber )
          .then( response => {
               commit('setActivities', response.data );
          });
     },
     getActivity( {commit}, id ) {
          axios.get("api/cat_activity/" + id)
          .then( response => {
               commit('setActivity', response.data );
          });
     }
};

const mutations = {
     setActivities( state, data ) {
          state.activities = data;
     },
     setCurrentPage( state, data ) {
          state.activities.current_page = data;
     },
     setActivity( state, data ) {
          state.activity = data;
     }
};

export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
}