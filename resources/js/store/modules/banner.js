import axios from "axios";

const state = {
     banners: {}
};

const getters = {};

const actions = {
     getList( {commit} ) {
          axios.get("api/cat_banners")
          .then( response => {
               commit('setBanners', response.data );
          });
     },
};

const mutations = {
     setBanners( state, data ) {
          state.banners = data;
     },
};

export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
}