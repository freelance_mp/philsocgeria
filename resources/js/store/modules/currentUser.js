import axios from "axios";

const state = {
     user: {}
};
const getters = {};
const actions = {
     getUser( {commit} ) {
          axios.get("/api/current_user")
          .then( response => {
               commit('setUser', response.data );
          })
          .catch( error => {
               console.log(error)
          });
     },
     loginUser( {}, user ) {
          axios.post("/api/login", {
               email: user.email,
               password: user.password
          })
          .then( response => {
               if( response.status === 200 ) {
                    if( response.data.token ) {
                         // store token to local storage
                         if( response.data.admin ) {
                              localStorage.setItem("app_token", response.data.token)
                              window.location.replace("/dashboard");
                         } else {
                              localStorage.setItem("mem_token", response.data.token)
                              window.location.replace("/home");
                         }
                    }
               }
          })
     },
     logoutUser() {
          // delete token from local storage
          localStorage.removeItem("app_token");
          localStorage.removeItem("mem_token");
          window.location.replace("/home");
     }
};
const mutations = {
     setUser( state, data ) {
          state.user = data;
     }
};

export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
}