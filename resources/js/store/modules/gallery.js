import axios from "axios";

const state = {
     albums: {},
     album: {}
};

const getters = {};

const actions = {
     getList( {commit}, pageNumber) {
          axios.get("api/gallery/cat_albums?page=" + pageNumber )
          .then( response => {
               commit('setAlbums', response.data );
          });
     },
     catalogGetList( {commit}, pageNumber) {
          axios.get("api/gallery/cat_albums?page=" + pageNumber )
          .then( response => {
               commit('setAlbums', response.data );
          });
     },
     getAlbum( {commit}, id ) {
          axios.get("api/gallery/cat_album/" + id)
          .then( response => {
               commit('setAlbum', response.data );
          });
     }
};

const mutations = {
     setAlbums( state, data ) {
          state.albums = data;
     },
     setCurrentPage( state, data ) {
          state.albums.current_page = data;
     },
     setAlbum( state, data ) {
          state.album = data;
     }
};

export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
}