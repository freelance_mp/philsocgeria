import axios from "axios";

const state = {
     deleteModalObj : {
          showDeleteModal: false,
          deleteUrl: '',
          data: null,
          deletingIndex: -1,
          isDeleted: false,
          deleteModal: true,
      },
};
const getters = {
     getDeleteModalObj(state) {
          return state.deleteModalObj
     }
};
const actions = {};
const mutations = {
     setDeleteModal(state, data){
          const deleteModalObj = {
              showDeleteModal: false,
              deleteUrl: '',
              data: null,
              deletingIndex: state.deleteModalObj.deletingIndex,
              isDeleted: data,
              deleteModal: true,
          }
          state.deleteModalObj = deleteModalObj
     },
     setDeletingModalObj(state, data){
          state.deleteModalObj = data
     },
};

export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
}