const mix = require('laravel-mix');

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

var webpackConfig = {
    plugins: [
        new VuetifyLoaderPlugin(),
        new CaseSensitivePathsPlugin()
    ]
}
mix.webpackConfig( webpackConfig );

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'public/css/slick.css',
    'public/css/slick-theme.css',
    'public/css/magnific-popup.css',
    'public/css/style.css',
], 'public/css/all.css');

mix.scripts([
    'resources/js/assets/jquery-3.4.1.js',
    'resources/js/assets/slick.min.js',
    'resources/js/assets/jquery.magnific-popup.js',
    'resources/js/assets/script.js',
], 'public/js/all.js');
