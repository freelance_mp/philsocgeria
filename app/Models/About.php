<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;
    protected $fillable = ['title'];

    public function about_sections(){
        return $this->hasMany(AboutSection::class);
    }

    public function about_panels(){
        return $this->hasMany(AboutPanel::class);
    }
}
