<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityImage extends Model
{
    use HasFactory;
    protected $fillable = [
        'image_path',
        'sort_order',
        'activity_id',
    ];

    public function activities() {
        return $this->belongsTo(Activity::class);
    }
}
