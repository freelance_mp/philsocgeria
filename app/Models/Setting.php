<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    protected $fillable = [
        'logo',
        'name',
        'email',
        'contact_number',
        'address',
        'google_map',
    ];

    public function social_media(){
        return $this->hasMany(SocialMedia::class);
    }

    public function partners(){
        return $this->hasMany(Partner::class);
    }
}
