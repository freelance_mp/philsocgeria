<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'description',
        'featured_image',
        'is_featured',
    ];
    
    public function activity_images(){
        return $this->hasMany(ActivityImage::class);
    }
}
