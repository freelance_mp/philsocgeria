<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutPanelSection extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'description', 'image_path', 'about_panel_id'];

    public function about_panel(){
        return $this->belongsTo(AboutPanel::class);
    }
}
