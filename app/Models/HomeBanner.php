<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeBanner extends Model
{
    use HasFactory;
    protected $fillable = [
        "title", "description", "theme", "text_position", "to_display", "image_path", "sort_order"
    ];
}
