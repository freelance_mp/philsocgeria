<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingsController extends Controller
{
    public function getSettings(){
        $settings = Setting::with('social_media', 'partners')->find(1);
        return response()->json( $settings, 200 );
    }

    public function saveSettings( $id, Request $request ){
        $settings = Setting::find( $id );

        $rules = [
            "logo" => "nullable",
            "name" => "required",
            "email" => "required",
            "contact_number" => "nullable|regex:/^[a-zA-Z0-9-.,+()\n ]+$/|max:255",
            "address" => "nullable|regex:/^[a-zA-Z0-9-.,+\n ]+$/|max:255",
            "google_map" => "nullable|max:1000",
        ];

        $this->validate($request, $rules);
        $destination = "/uploads/";

        if( str_contains( $request->logo, $destination ) ){
            $settings->logo = $request->logo;
        } else {
            $settings->logo = $destination.$request->logo;
        }
        $settings->name = $request->name;  
        $settings->email = $request->email;  
        $settings->contact_number = $request->contact_number;
        $settings->address = $request->address;  
        $settings->google_map = $request->google_map;  
        $settings->save();

        return response()->json( $settings, 200 );
    }
}
