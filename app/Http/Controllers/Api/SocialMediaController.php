<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SocialMedia;

class SocialMediaController extends Controller
{
    public function showAllSocials(){
        $socials = SocialMedia::orderBy('id','desc')->get();
        return $socials;     
    }

    public function addSocial( Request $request ){
        $rules = [
            "name" => "required",
            "link" => "required",
            "image_path" => "required",
        ];

        $this->validate( $request, $rules );
        $destination = "/uploads/";

        $new_social = new SocialMedia;
        $new_social->name = $request->name;
        $new_social->link = $request->link;
        $new_social->image_path = $destination.$request->image_path;
        $new_social->setting_id = $request->setting_id;

        $new_social->save();
        return response()->json( $new_social, 200 ); 
    }

    public function updateSocial( $id, Request $request ){
        $social = SocialMedia::find( $id );        
        $rules = [
            "name" => "required",
            "image_path" => "required",
            "link" => "required",
        ];

        $this->validate( $request, $rules );
        $destination = "/uploads/";

        $social->name = $request->name;
        $social->link = $request->link;
        if( str_contains( $request->image_path, $destination ) ){
            $social->image_path = $request->image_path;
        } else {
            $social->image_path = $destination.$request->image_path;
        }

        $social->save();
        return response()->json( $social, 200 );
    }

    public function deleteSocial( $id, Request $request ){
        $socialToDelete = SocialMedia::find( $id );
        $socialToDelete->delete();
        return response()->json( $socialToDelete, 200 );
    }
}
