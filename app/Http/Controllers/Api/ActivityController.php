<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\ActivityImage;

class ActivityController extends Controller
{
    public function getAllActivities() {
        $activities = Activity::with(['activity_images' => function($q) {
            $q->orderBy('created_at', 'asc');
        }])->orderBy('created_at', 'desc')->paginate(12);
        
        foreach( $activities as $activity ){
            $activity['created'] = $activity->created_at->diffForHumans();
            $activity['latest_image'] = ActivityImage::where('activity_id', $activity->id)->latest()->first();
        }

        return response()->json( $activities, 200 );
    }

    public function createActivity( Request $request ) {
        $rules = array(
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:3000',
        );
        $this->validate( $request, $rules );
        $destination = "/uploads/";
        $activity = Activity::create([
            'title' => $request->title,
            'description' => $request->description,
            'featured_image' => $request->filled('featured_image') ? $destination.$request->featured_image : '',
            'is_featured' => $request->is_featured ? $request->is_featured : 0,
        ]);

        $images = $request->get('images');
        $activity->activity_images()->createMany( $images );

        return response()->json( [$activity, 'msg' => 'Activity has been added.'], 200 );
    }

    public function updateActivity( $id, Request $request ) {
        $rules = array(
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:3000',
        );
        $this->validate( $request, $rules );
        $destination = "/uploads/";
        $activity = Activity::find($id);

        $activity->title = $request->title;
        $activity->description = $request->description;
        if( $request->filled('featured_image') ) {
            if( str_contains( $request->featured_image, $destination ) ){
                $activity->featured_image = $request->featured_image;
            } else {
                $activity->featured_image = $destination.$request->featured_image;
            }
        }
        $activity->is_featured = $request->is_featured;

        $images = $request->get('images');
        $activity->activity_images()->createMany( $images );
        $activity->save();

        return response()->json( [$activity, 'msg' => 'Activity has been updated.'], 200 );
    }

    public function deleteActivity( $id ) {
        $activity = Activity::find( $id );
        $activity->delete();
        
        return response()->json( ['msg' => 'Activity has been deleted.'], 200 );
    }

    // Catalog
    public function getFeaturedActivities() {
        $activities = Activity::with(['activity_images' => function($q) {
            $q->orderBy('created_at', 'asc');
        }])->orderBy('created_at', 'desc')->where('is_featured', 1)->get();

        foreach( $activities as $activity ){
            $activity['created'] = $activity->created_at->diffForHumans();
            $activity['latest_image'] = ActivityImage::where('activity_id', $activity->id)->latest()->first();
        }

        return response()->json( $activities, 200 );
    }

    public function getActivity( $id ) {
        $activity = Activity::with('activity_images')->find( $id );
        return response()->json( $activity, 200 );
    }

}