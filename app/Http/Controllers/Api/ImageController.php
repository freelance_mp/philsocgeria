<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ImageController extends Controller
{
    public function uploadImage( Request $request ) {
        $rules = array(
            "file" => "required|image|mimes:jpeg,jpg,png|max:2048",
        );
        $this->validate($request, $rules);
        $random = Str::random(5);
        $image = $request->file;
        $image_name = $random . time() . "." . $image->extension();
        $image->move(public_path("uploads"), $image_name);

        return $image_name;
    }

    public function uploadFile( Request $request ) {
        $rules = array(
            "file" => "required|mimetypes:application/pdf,video/mp4,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv|max:20000",
        );
        $this->validate($request, $rules);
        $random = Str::random(5);
        $file = $request->file;
        $file_name = $random . time() . "." . $file->extension();
        $file->move(public_path("uploads"), $file_name);

        return $file_name;
    }

    public function uploadMultipleImage( Request $request ) {
        $rules = array(
            "file" => "required|image|mimes:jpeg,jpg,png|max:2048",
        );
        $this->validate($request, $rules);
        $random = Str::random(5);
        $image = $request->file;
        $image_name = $random . time() . "." . $image->extension();
        $image->move(public_path("uploads"), $image_name);
        $image_details = array(
            'image_path' => '/uploads/'.$image_name,
            'sort_order' => 0,
        );

        return response()->json( $image_details, 200 ); 
    }

    public function deleteFile( Request $request ) {
        $file_name = $request->imageName;
        $destination = '/uploads/';
        $full_path = str_contains( $file_name, $destination ) ? true : false;
        $this->deleteFileFromServer( $file_name, $full_path );
        return response()->json( ['msg' => 'Image has been removed from server.'], 200 );
    }

    public function deleteFileFromServer( $file, $full_path ) {
        if( !$full_path ) {
            $file_path = public_path().'/uploads/'.$file;
        } else {
            $file_path = public_path().$file;
        }
        
        if( file_exists($file_path) ) {
            @unlink($file_path);
        }
        return;
    }
}
