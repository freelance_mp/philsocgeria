<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function register( Request $request ) {
        $fields = $request->validate([
            'first'     =>  'required|string|max:255',
            'last'      =>  'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users,email',
            'password'  => 'required|string|min:8|confirmed',
            'is_admin'  => 'required|boolean',
        ]);

        $user = User::create([
            'first'     => $fields['first'],
            'last'      => $fields['last'],
            'email'     => $fields['email'],
            'password'  => Hash::make($fields['password']),
            'is_admin'  => $fields['is_admin'],
        ]);

        $token = $user->createToken('apptoken')->plainTextToken;
        $res = [
            'user' => $user,
            'token' => $token,
            'msg' => 'Registered Successfully'
        ];

        return response()->json($res, 200);
    }

    public function login( Request $request ) {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        // Check email
        $user = User::where('email', $fields['email'])->first();

        // Check password
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            // throw ValidationException::withMessages([
            //     'email' => 'Invalid login credentials.'
            // ]);
            return response(['msg' => 'Invalid login credentials.'], 401);
        }

        $token = $user->createToken('apptoken')->plainTextToken;
        $res = [
            'admin' => $user->is_admin,
            'user' => $user,
            'token' => $token,
            'msg' => 'You are now logged in.'
        ];

        return response()->json($res, 200);
    }

    public function logout( Request $request ) {
        auth()->user()->tokens()->delete();

        return ['message' => 'Logged Out'];
    }
}
