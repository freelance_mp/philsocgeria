<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BoardMember;

class BoardMemberController extends Controller
{
    public function getAllMembers(){
        return BoardMember::all();
    }

    public function createMember(Request $request){
        $rules = array(
            "name" => "required",
            "position" => "required",
            "description" => "nullable"
        );
        
        $this->validate($request, $rules);
        $destination = "/uploads/";

        $new_member = new BoardMember;

        
        $new_member->name = $request->name;
        $new_member->position = $request->position;
        $new_member->description = $request->description;
        if( $request->filled('avatar') ) {
            $new_member->image_path = $destination.$request->avatar;
        }
        $new_member->save();
        return response()->json($new_member,200);     
    }

    public function updateMember($id, Request $request) {
        $rules = array(
            "name" => "required",
            "position" => "required",
            "description" => "nullable"
        );

        $this->validate($request, $rules);
        $destination = "/uploads/";
        $member = BoardMember::find($id);

        $member->name = $request->name;
        $member->position = $request->position;
        $member->description = $request->description;  
        if( $request->filled('avatar') ) {
            if( str_contains( $request->avatar, $destination ) ){
                $member->image_path = $request->avatar;
            } else {
                $member->image_path = $destination.$request->avatar;
            }
        }
        $member->save();
        return response()->json($member,200);
    }

    public function deleteMember($id) {
        $member = BoardMember::find($id);
        $member->delete();
        return response()->json(["msg" => "Member has been deleted"], 200);
    }
}
