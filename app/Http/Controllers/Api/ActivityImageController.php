<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\ActivityImage;

class ActivityImageController extends Controller
{
    public function createImage( Request $request ) {
        $activity = Activity::find( $request->id );
        $images = $request->get('images');
        $activity->gallery_images()->createMany( $images );

        return response()->json( $activity, 200 );
    }

    public function deleteImage( $id ) {
        $image = ActivityImage::find( $id );
        $image->delete();

        return response()->json( ['msg' => 'Image has been deleted.'], 200 );
    }
}
