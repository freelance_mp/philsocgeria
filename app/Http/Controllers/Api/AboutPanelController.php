<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutPanel;
use App\Models\AboutPanelSection;

class AboutPanelController extends Controller
{
    // About Panels
    public function getAllPanels() {
        $panels = AboutPanel::with('about_panel_sections')->orderBy('created_at','asc')->get();
        return response()->json( $panels, 200 );
    }

    public function createPanel( Request $request ) {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|min:5|max:191',        
        );
        $this->validate( $request, $rules );

        $panel = new AboutPanel;
        $panel->title = $request->title;
        $panel->about_id = 1;
        $panel->save();

        return response()->json( $panel, 200 );
    }

    public function updatePanel( $id, Request $request ) {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|min:5|max:191',        
        );
        $this->validate( $request, $rules );

        $panel = AboutPanel::find( $id );
        $panel->title = $request->title;
        $panel->save();

        return response()->json( $panel, 200 );
    }

    public function deletePanel( $id ) {
        $panel = AboutPanel::find( $id );
        $panel->delete();

        return response()->json( ['msg' => 'Panel has been deleted.'], 200 );
    }

    // About Panel Sections
    public function createPanelSection( $id, Request $request ) {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|min:5|max:191',
            'description' => 'nullable|max:3000',            
        );
        $this->validate( $request, $rules );
        $destination = "/uploads/";

        $panel = AboutPanel::find( $id );
        $panel->about_panel_sections()->create([
            'title' => $request->title,
            'description' => $request->description,
            'image_path' => $request->filled('image_path') ? $destination.$request->image_path : '',
            'about_id' => 1,
        ]);

        return response()->json( $panel, 200 );
    }

    public function updatePanelSection( $id, Request $request ) {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|min:5|max:191',
            'description' => 'nullable|max:3000',            
        );
        $this->validate( $request, $rules );
        $destination = "/uploads/";
        $section = AboutPanelSection::find( $id );
        $section->title = $request->title;
        $section->description = $request->description;
        if( $request->filled('image_path') ) {
            if( str_contains( $request->image_path, $destination ) ){
                $section->image_path = $request->image_path;
            } else {
                $section->image_path = $destination.$request->image_path;
            }
        }
        $section->save();

        return response()->json( $section, 200 );
    }

    public function deletePanelSection( $id ) {
        $section = AboutPanelSection::find( $id );
        $section->delete();

        return response()->json( ['msg' => 'Panel section has been deleted.'], 200 );
    }
}
