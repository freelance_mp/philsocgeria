<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function getAllUsers() {
        $users = User::orderBy('created_at','desc')->paginate(10);
        return response()->json($users, 200);
    }

    public function currentUser() {
        $currentUser =  Auth::user();
        $logged = false;
        if(Auth::user()){
            $logged = true;
        }
        $currentUser['logged'] = $logged;
        return response()->json($currentUser);
    }
    
    public function updateUser( $id, Request $request ) {
        $rules = array(
            'first'     =>  "required|string|max:255",
            'last'      =>  "required|string|max:255",
            'email'     => "required|string|email|max:255|unique:users,email,$request->id",
            'is_admin'  => "required|boolean",
        );

        $this->validate($request, $rules);

        $user = User::find( $id );
        $user->first = $request->first;
        $user->last = $request->last;
        $user->email = $request->email;
        $user->is_admin = $request->is_admin;

        $user->save();
        
        $res = [
            'user' => $user,
            'msg' => 'Registered Successfully'
        ];

        return response()->json($res, 200);
    }

    public function deleteUser( $id ) {
        $userToDelete = User::find( $id );
        $userToDelete->delete();
        return response()->json(['msg', 'User account has been deleted.'], 200);
    }

    // Profile
    public function updateProfilePassword( $id, Request $request ) {
        $fields = $request->validate([
            'email' => 'required|string',
            'old_password' => 'required|string',
            'password'  => 'required|string|min:8|confirmed',
        ]);

        // Check email
        $user = User::where('email', $fields['email'])->first();

        // Check password
        if(!$user || !Hash::check($fields['old_password'], $user->password)) {
            // throw ValidationException::withMessages([
            //     'email' => 'Invalid credentials.'
            // ]);
            return response()->json(['msg' => 'Invalid credentials.'], 401);
        } else {
            $user->password = Hash::make($fields['password']);
            $user->save();
        }

        return response()->json( ['msg' => 'Your password has been updated.'], 200);
    }

    public function updateProfile( $id, Request $request ) {
        $rules = array(
            'first'     =>  "required|string|max:255",
            'last'      =>  "required|string|max:255",
        );

        $this->validate($request, $rules);
        $destination = "/uploads/";
        $user = User::find($id);

        $user->first = $request->first;
        $user->last = $request->last;
        if( $request->filled('avatar') ) {
            if( str_contains( $request->avatar, $destination ) ){
                $user->avatar = $request->avatar;
            } else {
                $user->avatar = $destination.$request->avatar;
            }
        }
        $user->save();
        return response()->json($user,200);
    }
}
