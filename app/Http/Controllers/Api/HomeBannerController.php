<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HomeBanner;

class HomeBannerController extends Controller
{
    public function getAllBanners(){
        return HomeBanner::all();
    }

    public function createBanner(Request $request){
        $rules = array(
            "title" => "required|string",
            "image_path" => "required",
            "theme" => "required",
            "text_position" => "required",
            "display" => "boolean",
            "sort_order" => "nullable|integer",
        );
        
        $this->validate($request, $rules);
        $destination = "/uploads/";

        $new_banner = new HomeBanner;
        
        $new_banner->title = $request->title;
        $new_banner->description = $request->description;
        $new_banner->theme = $request->theme;
        $new_banner->text_position = $request->text_position;
        $new_banner->to_display = $request->to_display;
        $new_banner->image_path = $destination.$request->image_path;
        $new_banner->sort_order = $request->sort_order ? $request->sort_order : 0;
 
        $new_banner->save();
        return response()->json($new_banner,200);     
    }

    public function updateBanner($id, Request $request) {
        $rules = array(
            "title" => "required|string",
            "image_path" => "required",
            "theme" => "required",
            "text_position" => "required",
            "display" => "boolean",
            "sort_order" => "nullable|integer",
        );

        $this->validate($request, $rules);
        $destination = "/uploads/";
        $banner = HomeBanner::find($id);

        $banner->title = $request->title;
        $banner->description = $request->description;
        $banner->theme = $request->theme;
        $banner->text_position = $request->text_position;
        $banner->to_display = $request->to_display;
        if( str_contains( $request->image_path, $destination ) ){
            $banner->image_path = $request->image_path;
        } else {
            $banner->image_path = $destination.$request->image_path;
        }
        $banner->sort_order = $request->sort_order ? $request->sort_order : 0;

        $banner->save();
        return response()->json($banner,200);
    }

    public function deleteBanner($id) {
        $banner = HomeBanner::find($id);
        $banner->delete();
        return response()->json(["msg" => "Banner has been deleted"], 200);
    }

    public function getHomeBanners() {
        return HomeBanner::orderBy('sort_order', 'asc')->get();
    }
}
