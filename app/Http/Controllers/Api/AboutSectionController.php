<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\About;
use App\Models\AboutSection;

class AboutSectionController extends Controller
{
    public function getAllSections() {
        $sections = AboutSection::all();
        return response()->json( $sections, 200 );
    }

    public function createSection( Request $request ) {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|min:5|max:191',
            'description' => 'nullable|max:3000',            
        );
        $this->validate( $request, $rules );
        $destination = "/uploads/";

        $section = new AboutSection;
        $section->title = $request->title;
        $section->description = $request->description;
        $section->image_path = $request->filled('image_path') ? $destination.$request->image_path : '';
        $section->about_id = 1;
        $section->save();

        return response()->json( $section, 200 );
    }

    public function updateSection( $id, Request $request ) {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|min:5|max:191',
            'description' => 'nullable|max:3000',            
        );
        $this->validate( $request, $rules );
        $destination = "/uploads/";

        $section = AboutSection::find( $id );
        $section->title = $request->title;
        $section->description = $request->description;
        if( $request->filled('image_path') ) {
            if( str_contains( $request->image_path, $destination ) ){
                $section->image_path = $request->image_path;
            } else {
                $section->image_path = $destination.$request->image_path;
            }
        }
        $section->save();

        return response()->json( $section, 200 );
    }

    public function deleteSection( $id ) {
        $section = AboutSection::find( $id );
        $section->delete();

        return response()->json( ['msg' => 'Section has been deleted.'], 200 );
    }
}
