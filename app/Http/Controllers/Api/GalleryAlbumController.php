<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GalleryAlbum;
use App\Models\GalleryImage;

class GalleryAlbumController extends Controller
{
    public function getAllAlbums() {
        $albums = GalleryAlbum::orderBy( 'created_at', 'desc' )->get();
        return response()->json( $albums, 200 );
    }

    public function getAlbum( $id ) {
        $album = GalleryAlbum::with(['gallery_images' => function($q) {
            $q->orderBy('created_at', 'asc');
        }])->where( 'id', $id )->first();

        return response()->json( $album, 200 );
    }

    public function createAlbum( Request $request ) {
        $rules = array(
            "name" => "required|string|min:6",
            "description" => "nullable|string|max:3000"
        );
        
        $this->validate( $request, $rules );
        $destination = "/uploads/";

        $new_album = new GalleryAlbum;

        
        $new_album->name = $request->name;
        $new_album->description = $request->description;
        if( $request->filled('image_path') ) {
            $new_album->image_path = $destination.$request->image_path;
        } else {
            $new_album->image_path = '';
        }
        $new_album->save();
        return response()->json($new_album,200);
    }

    public function updateAlbum( $id, Request $request ) {
        $rules = array(
            "name" => "required|string|min:6",
            "description" => "nullable|string|max:3000"
        );

        $this->validate( $request, $rules );
        $destination = "/uploads/";
        $album = GalleryAlbum::find( $id );

        $album->name = $request->name;
        $album->description = $request->description;  
        if( $request->filled('image_path') ) {
            if( str_contains( $request->image_path, $destination ) ){
                $album->image_path = $request->image_path;
            } else {
                $album->image_path = $destination.$request->image_path;
            }
        }

        $album->save();
        return response()->json( $album, 200 );
    }

    public function deleteAlbum( $id ) {
        $album = GalleryAlbum::find( $id );
        $album->delete();
        return response()->json( ["msg" => "Album has been deleted"], 200 );
    }

    // Catalog
    public function getAlbums() {
        $albums = GalleryAlbum::orderBy( 'created_at', 'desc' )->paginate( 12 );
        return response()->json( $albums, 200 );
    }
}
