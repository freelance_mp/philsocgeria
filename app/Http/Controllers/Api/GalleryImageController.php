<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GalleryAlbum;
use App\Models\GalleryImage;

class GalleryImageController extends Controller
{
    public function createImage( Request $request ) {
        $album = GalleryAlbum::find( $request->id );
        $images = $request->get('images');
        $album->gallery_images()->createMany( $images );

        return response()->json( $album, 200 );
    }

    public function deleteImage( $id ) {
        $image = GalleryImage::find( $id );
        $image->delete();

        return response()->json( ['msg' => 'Image has been deleted.'], 200 );
    }
}
