<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Education;

class EducationController extends Controller
{
    public function getAllLessons() {
        $lessons = Education::orderBy('created_at', 'desc')->paginate(2);

        return response()->json( $lessons, 200 );
    }

    public function createLesson( Request $request ) {
        $rules = array(
            "title" => "required|string",
            "description" => "nullable|string",
            "file" => "required",
        );
        
        $this->validate( $request, $rules );

        $destination = "/uploads/";
        
        $new_lesson = new Education;        
        $new_lesson->title = $request->title;
        $new_lesson->description = $request->description;
        $new_lesson->featured_image = $destination.$request->featured_image;
        $new_lesson->file_type = $request->file_type;

        // $file_extension = $request->file->extension();
        $is_pdf = str_contains( $request->file, 'pdf' ) ? true : false;
        if( $request->file_type == 'pdf' ) {
            if( !$is_pdf ) {
                return response()->json( ['error' => 'The file must be in pdf format.'], 422 );
            } else {
                $new_lesson->file = $destination.$request->file;
            }
        } else {
            if( $is_pdf ) {
                return response()->json( ['error' => 'The file must be in video format.'], 422 );
            } else {
                $new_lesson->file = $destination.$request->file;
            }
        }

        $new_lesson->save();
        return response()->json( $new_lesson, 200 );

    }
    
    public function updateLesson( $id, Request $request ) {
        $rules = array(
            "title" => "required|string",
            "description" => "nullable|string",
            "file" => "required",
        );
        
        $this->validate( $request, $rules );

        $destination = "/uploads/";
        
        $lesson = Education::find($id);        
        $lesson->title = $request->title;
        $lesson->description = $request->description;
        if( $request->filled('featured_image') ) {
            if( str_contains( $request->featured_image, $destination ) ){
                $lesson->featured_image = $request->featured_image;
            } else {
                $lesson->featured_image = $destination.$request->featured_image;
            }
        }
        $lesson->file_type = $request->file_type;
        if( $request->filled('file') ) {
            $is_pdf = str_contains( $request->file, 'pdf' ) ? true : false;
            if( $request->file_type == 'pdf' ) {
                if( !$is_pdf ) {
                    return response()->json( ['error' => 'The file must be in pdf format.'], 422 );
                } else {
                    if( str_contains( $request->file, $destination ) ){
                        $lesson->file = $request->file;
                    } else {
                        $lesson->file = $destination.$request->file;
                    }
                }
            } else {
                if( $is_pdf ) {
                    return response()->json( ['error' => 'The file must be in video format.'], 422 );
                } else {
                    if( str_contains( $request->file, $destination ) ){
                        $lesson->file = $request->file;
                    } else {
                        $lesson->file = $destination.$request->file;
                    }
                }
            }
        }

        $lesson->save();
        return response()->json( $lesson, 200 );
    }

    public function getLesson( $id ) {
        $lesson = Education::find($id);

        return response()->json( $lesson, 200 );
    }

    public function deleteLesson( $id ) {
        $lesson = Education::find( $id );
        $lesson->delete();
        
        return response()->json( ['msg' => 'Lesson has been deleted.'], 200 );
    }
}
